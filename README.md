# Openstack course template #
- creates and joins VMs to ups.ucn.muni.cz AD domain.
- assigns RDP access to users based on json definitions in student list- https://gitlab.ics.muni.cz/255879/openstack/blob/master/Powershell/

## Image creation for the course
- heat template works with cloudbase windows2012 image
- **!!! Image must be made from image, which is not connected to domain (not created with heat) !!!**, otherwise all servers will share the same hostname and cloudbase-init wont run correctly


## Manual - Create stack ##
1. Log in to https://ostack.ics.muni.cz
2. Choose course project
3. Go to Orchestration -> Stacks
4. Launch stack
5. Fill data
    * Template source - URL
    * Template URL - https://gitlab.ics.muni.cz/255879/openstack/raw/master/Templates/JSON/stack.json
    * Environment Source - Direct Input
    * Environment Data - Copy content of https://gitlab.ics.muni.cz/255879/openstack/blob/master/Templates/JSON/pv178-env.json

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/1.PNG)

6. Fill Data
    * Stack Name - Name of the course
    * Creation Timeout - 60
    * Rollback On Failure - unchecked
    * Password for user - Secondary password
    * Administrator password - <pw> for local administrator account
    * Name of the course - pv178 (only 'pv178' or pb007 allowed!)
    * Domain password - <pw> of user which adds computers to ups.ucn.muni.cz domain
    * Floating network - pritvate-1  
    * Image - image to be used for instances (selection from project)      
    * Flavor - 4cpu6ram80disk (instance size)      
    * Number of servers - <int> number of instances - can be changed later
    * Batch size - <int> number of instances created at once (1-10)
    * Network - internal-net (instances network private network)
    * Teacher password - <pw> of local user 'teacher' with administrator rights

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/2.PNG)

7. Check status of created stack

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/3.PNG)


## Manual - Change stack configuration ##

1. Log in to https://ostack.ics.muni.cz
2. Choose PB007 project
3. Go to Orchestration -> Stacks
4. Choose stack and click "Change Stack Template"

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/4.PNG)

5. Fill new data
6. Check status of operation

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/5.PNG)

## Rebuild malfunctioning virtual machine ##

1. Log in to https://ostack.ics.muni.cz
2. Choose PB007 project
3. Go to Instances
4. Select malfunctioning virtual machine and click "Rebuild Instance"

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/6.PNG)

5. Select corresponding image ("PB007") and click "Rebuild Instance"

    ![Launch stack](https://gitlab.ics.muni.cz/255879/openstack/raw/master/Manual/7.PNG)
